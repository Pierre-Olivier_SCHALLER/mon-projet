## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install foobar
```

## Usage

```python
import foobar

# returns 'server'
foobar.pluralize('server')

# returns 'geese'
foobar.pluralize('goose')

# returns 'server'
foobar.singularize('server')
```